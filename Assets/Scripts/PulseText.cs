﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PulseText : MonoBehaviour
{
    Text text;
    public float fadeDuration;

    private void Start()
    {
        text = GetComponent<Text>();
        text.color = Color.white;
        StartCoroutine(Fade());
    }
   
    public IEnumerator Fade()
    {
        while (enabled)
        {
            for (float time = 0; time < fadeDuration; time += Time.deltaTime)
            {
                float frac = time / fadeDuration;
                text.color = Color.Lerp(Color.white, new Color(1, 1, 1, 0), 1.1f);
                yield return new WaitForEndOfFrame();
            }
            for (float time = 0; time < fadeDuration; time += Time.deltaTime)
            {
                float frac = time / fadeDuration;
                text.color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, 1.1f);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
